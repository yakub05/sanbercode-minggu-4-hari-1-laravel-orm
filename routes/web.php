<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//CRUD cast
Route::get('/cast', 'CastController@index');
Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');
Route::get('/cast/{cast_id}','CastController@show');
Route::put('/cast/{cast_id}','CastController@update');
Route::get('/cast/{cast_id}/edit','CastController@edit');
Route::delete('/cast/{cast_id}', 'CastController@destroy');

//CRUD Film
Route::get('/film/create', 'FilmController@create');
Route::get('/film', 'FilmController@index');
Route::post('/film', 'FilmController@store');
Route::get('/film/{film_id}', 'FilmController@show');
Route::get('/film/{film_id}/edit','FilmController@edit');
Route::put('/film/{film_id}', 'FilmController@update');
Route::delete('/film/{film_id}', 'FilmController@destroy');

