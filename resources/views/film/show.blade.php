@extends('layout.master')
@section('judul')
    Halaman Detail Film {{$film->judul}}
@endsection
@section('isi')
<a href="/film" class="btn btn-danger my-2">Kembali</a>
<div class="row">
<div class="col">
    <div class="card">
        <img src="{{asset('poster/'.$film->poster)}}" class="my-2" alt="" style="width:300px; height:400px">
          <h2>{{$film->judul}} ({{$film->tahun}})</h2>
          <p>{{$film->ringkasan}}</p>
      </div>
</div>
@endsection